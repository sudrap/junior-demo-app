//
//  DetailView.swift
//  demo
//
//  Created by Josef Pardus on 16.09.2021.
//

import UIKit

class DetailView: UIViewController {
    
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var imageType: UIImageView!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var date: UILabel!
    
    var transaction:Transaction?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        background.image = UIImage(named: "Rectangle")
        
        if let safeTransaction = transaction {
            
        let transactionImage = "\(safeTransaction.type)_icon"
            imageType.image = UIImage(named: transactionImage)
            desc.text = safeTransaction.description
            type.text = safeTransaction.type
            amount.text = String(safeTransaction.amount)
            let formatedDate = safeTransaction.date.components(separatedBy: "T")
            date.text = formatedDate[0]
            self.navigationController?.isNavigationBarHidden = false
        }

        
    }
    

    
}
