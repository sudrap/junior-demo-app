//
//  ViewController.swift
//  demo
//
//  Created by Josef Pardus on 16.09.2021.
//

import UIKit

class MainPage: UITableViewController {
    var manager = DataManager()
    var transactions: [Transaction]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "Rectangle"))
        self.tableView.backgroundColor = .none
        manager.delegate = self
        manager.fetchData()
        
       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: Tableview dataSource methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  transactions?.count ?? 0
        
        
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ShopCell.identifier, for: indexPath) as? ShopCell else { return UITableViewCell()
        }
        
        if let transaction = transactions?[indexPath.row] {
            DispatchQueue.main.async {
                cell.category.text = transaction.type
                let transactionImage = "\(transaction.type)_icon"
                cell.icon.image = UIImage(named: transactionImage)
                cell.amount.text = String(transaction.amount)
                cell.desc.text = transaction.description
                cell.backgroundColor = .clear
            }

        }
        return cell
    }
    // MARK: - TableView delegate methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = storyboard.instantiateViewController(identifier: "Detail") as! DetailView
        if let transaction = transactions?[indexPath.row] {
            print("typ transakce\(transaction.type)")
            secondVC.transaction = transaction
            show(secondVC, sender: self)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension MainPage: ShopManagerDelegate {
    
    func didGetData(_ manager: DataManager, data: [Transaction]) {
        transactions = data
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
}
