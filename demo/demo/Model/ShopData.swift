//
//  ShopData.swift
//  demo
//
//  Created by Josef Pardus on 16.09.2021.
//

import Foundation
import UIKit

struct ShopData: Decodable {
    
    let transactions: [Transaction]
}
struct Transaction : Decodable {
    
    let id : Int
    let type: String
    let description: String
    let date: String
    let amount: Double
}
