//
//  DataManager.swift
//  demo
//
//  Created by Josef Pardus on 16.09.2021.
//

import Foundation
import UIKit


protocol ShopManagerDelegate {
    func didGetData (_ manager: DataManager, data: [Transaction])
}
struct DataManager {
    
    var delegate: ShopManagerDelegate?
    
    func fetchData () {
        let url = URL(string: "https://stub.bbeight.synetech.cz/v1/customers/3")!
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { data, response, error in
            handleData(data: data, response: response, error: error)
        }
        task.resume()
    }
    
   private func handleData (data:Data?, response:URLResponse?, error:Error?) {
        if error != nil {
            print(error!)
        }
        if let safeData = data {
            if let transactions = parseJson(safeData) {
                
                self.delegate?.didGetData(self, data: transactions)
            }
         
        }
        else {
            print("something went wrong with data")
        }
    }
    
    func parseJson (_ data: Data) -> [Transaction]? {
        let decoder = JSONDecoder()
        do {
        let data = try decoder.decode(ShopData.self, from: data)
            return data.transactions
        }catch {
            print("there was an error decoding data")
            return nil
        }
    }
}
