//
//  ShopCell.swift
//  demo
//
//  Created by Josef Pardus on 16.09.2021.
//

import Foundation
import UIKit

class ShopCell: UITableViewCell {
    
    static let identifier = "ShopCell"
    @IBOutlet var icon : UIImageView!
    @IBOutlet var desc : UILabel!
    @IBOutlet var category: UILabel!
    @IBOutlet var amount: UILabel!
    
    }
    

